# Galaxy Classifier project
## Getting started in machine learning projects

This project aims at getting started with a specific machine learning project. 
In the light of the large amount of data being collected everyday, and all the
insight that we can gain from it, we wanted to start with a project that would 
provide us with the main ideas on how to build these up.

The galaxy calssifier project is suitable for us, as we have prior knowledge
in the field of Astronomy, and this is the main reason behind our idea. We will
try to use our expertise to gain the sufficient knowledge to be able to 
replicate these ideas on other projects as well.